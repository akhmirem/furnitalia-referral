import Rebase from 're-base';
import firebase from 'firebase';
import 'firebase/firestore'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyBiK5J23tPEYwGFwQn4V6F1M6TmiSqF11o",
    authDomain: "furnitalia-referrals.firebaseapp.com",
    databaseURL: "https://furnitalia-referrals.firebaseio.com",
    projectId: "furnitalia-referrals",
    storageBucket: "furnitalia-referrals.appspot.com",
    messagingSenderId: "941525003249"
});

// const base = Rebase.createClass(firebaseApp.database());
// const base = Rebase.createClass(firebaseApp.firestore());

const db = firebase.firestore(firebaseApp);

const base = Rebase.createClass(db);

export {firebaseApp};

export default base;
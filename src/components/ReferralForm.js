import React from "react";
import PropTypes from "prop-types";
import Button from '@material-ui/core/Button';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import withStyles from "@material-ui/core/es/styles/withStyles";


const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

function ReferralForm(props) {


    return (
        <form className="referral-entry" onSubmit={props.onSubmit}>

            <h3>Referrals</h3>
            {/*<h4>{props.customer.firstName ? props.customer.firstName : ''}</h4>*/}

            {props.customer.referrals.map((referral, idx) => (
                    <div className="referral" key={`referral${idx}`}>
                        <FormControl margin="normal" required>
                            <InputLabel htmlFor={`name${idx}`}>Name</InputLabel>
                            <Input
                                autoFocus
                                placeholder={`Friend\'s #${idx + 1} name`}
                                defaultValue={referral.name}
                                onChange={props.onUpdateReferral(idx, 'name')}
                                key={`name${idx}`}
                                id={`name${idx}`}
                                name={`name${idx}`}
                            />
                        </FormControl>
                        <FormControl margin="normal" required>
                            <InputLabel htmlFor={`email${idx}`}>E-mail</InputLabel>
                            <Input
                                type="email"
                                placeholder={`Friend\'s #${idx + 1} email`}
                                defaultValue={referral.email}
                                onChange={props.onUpdateReferral(idx, 'email')}
                                key={`email${idx}`}
                            />
                        </FormControl>
                        <button type="button" onClick={props.onRemoveReferral(idx)} className="small">-</button>
                    </div>
                )
            )}

            <Button variant="contained" onClick={props.onAddReferral} className="small">Add another</Button>

            <Button variant="contained" color="primary" type="submit" fullWidth>Send</Button>

        </form>
    );


}

ReferralForm.propTypes = {
    addReferral: PropTypes.func,
    customer: PropTypes.object
};

export default withStyles(styles)(ReferralForm);

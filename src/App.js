import React, {Component} from 'react';
import './App.css';
import ReferralForm from "./components/ReferralForm";
import PropTypes from "prop-types";
import base from "./base";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";

class App extends Component {

    state = {
        referralCode: '',
        customer: {},
        loading: true
    };

    static propTypes = {
        referralCode: PropTypes.string
    };

    componentDidMount() {
        const referralCode = App.getUrlParameter('referralCode'); // this.props.match;

        this.setState({referralCode: referralCode});

        const localStorageRef = localStorage.getItem(referralCode);
        if (localStorageRef) {
            this.setState({customer: JSON.parse(localStorageRef)});
        }


        base.get('customers', {
            context: this,
            asArray: true,
            withRefs: true,
            withIds: true,
            query: ref => {
                return ref
                    .where('referralCode', '==', referralCode).limit(1)
            },
        }).then(data => {

            // if customer was found by referral code
            if (data.length === 1) {

                const docRef = data[0].ref;
                this.ref = base.syncDoc(docRef, {
                    context: this,
                    state: 'customer',
                    then() {
                        const tempCustomer = this.state.customer;
                        if (typeof tempCustomer.referrals === "undefined"
                            || tempCustomer.referrals.length === 0) {

                            tempCustomer.referrals = Array({'email': 'email@test.com', 'name': 'Friend\'s name'});
                            this.setState({loading: false, customer: tempCustomer});

                        } else {
                            this.setState({loading: false});
                        }

                    },
                    onFailure(err) {
                        console.log(err);
                    }
                })
            }
        }).catch(err => {
            console.log(err);
        });


        /*this.ref = base.bindCollection('customers', {
            context: this,
            state: 'customers',
            withRefs: true,
            query: ref => {
                return ref
                    .where('referralCode', '==', referralCode).limit(1)
            },
            then() {
                this.setState({ loading: false });
            }
        });*/

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // localStorage.setItem(App.getUrlParameter('referralCode'), JSON.stringify(this.state.referrals));
    }

    componentWillUnmount() {
        base.removeBinding(this.ref);
    }

    handleReferralInfoChange = (idx, type) => (evt) => {
        const newReferrals = this.state.customer.referrals.map((referral, sidx) => {
            if (idx !== sidx) return referral;
            console.log('type ' + type );
            if (type === 'name') {
                console.log(evt.target.value);
                return {...referral, name: evt.target.value};
            } else if (type === 'email') {
                return {...referral, email: evt.target.value};
            }
        });

        const newCustomer = {...this.state.customer, referrals: newReferrals};

        this.setState({customer: newCustomer});
    };

    handleSubmit = (evt) => {
        const {firstName, referrals} = this.state.customer;
        alert(`Customer: ${firstName} referred ${referrals.length} friends`);
    };

    handleAddReferral = () => {
        const newCustomer = this.state.customer;
        newCustomer.referrals = newCustomer.referrals.concat([{name: '', email: ''}]);
        this.setState({customer: newCustomer});
    };

    handleRemoveReferral = (idx) => () => {
        const newCustomer = this.state.customer;
        newCustomer.referrals = newCustomer.referrals.filter((s, sidx) => idx !== sidx);
        this.setState({customer: newCustomer});
    };

    render() {
        return (
            <main className="App">

                <CssBaseline />

                <Paper>
                Refer your friends and get a FREE Natuzzi Leather Kit when they purchase from Furnitalia.
                They will get one too.

                <h3>Here's How It Works:</h3>
                <ul>
                    <li>Send referrals to your friends via the form below</li>
                    <li>Both you and your friends will receive a Natuzzi Leather Kit upon purchase</li>
                </ul>

                {this.state.loading ? (
                    ''
                ) : (
                    <ReferralForm
                        customer={this.state.customer}
                        onSubmit={this.handleSubmit}
                        onAddReferral={this.handleAddReferral}
                        onRemoveReferral={this.handleRemoveReferral}
                        onUpdateReferral={this.handleReferralInfoChange}
                    />
                )}

                </Paper>
            </main>
        );
    }

    static getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(window.location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
}

/*<!--
               1 Invite sent!
Your friend will be receiving their invite shortly.
               -->*/

export default App;
